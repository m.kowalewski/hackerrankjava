package pl.mkowalewski;

import java.util.Scanner;

public class JavaRegex {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        MyRegex regexProcessor = new MyRegex();
        String line;
        while(scanner.hasNextLine() && !((line = scanner.nextLine()).equals(""))){
            System.out.println(regexProcessor.testIp(line));
        }
        scanner.close();
    }

    private static class MyRegex {

        private String pattern = "^([0-9]|[0-9][0-9]|[0-1][0-9][0-9]|2[0-4][0-9]|25[0-5])\\.([0-9]|[0-9][0-9]|[0-1][0-9][0-9]|2[0-4][0-9]|25[0-5])\\.([0-9]|[0-9][0-9]|[0-1][0-9][0-9]|2[0-4][0-9]|25[0-5])\\.([0-9]|[0-9][0-9]|[0-1][0-9][0-9]|2[0-4][0-9]|25[0-5])$";

        boolean testIp(String ip) {
            return ip.matches(pattern);
        }

        public MyRegex() {}

    }

}
